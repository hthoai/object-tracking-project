# Proposal and Timeline for Object Tracking Project

## Project Overview
Use SIFT feature and Mean Shift to track a specific object in video.

## Goals
- Read and understand related papers.
- Reimplement the chosen method.
- Propose an improvement if can.

## Measurements of Success
- The result of SIFT and Mean Shift are quite similar to the OpenCV ones.
- All members meet their deadlines.

## Timeline
```mermaid
gantt
dateFormat  DD-MM-YYYY
title Milestone Plan

section Done 
Preparation             :mil1, 25-04-2019,8d
Sprint-2504              :spr2504, 25-04-2019,4d
Sprint-2904              :spr2904, after spr2504, 4d
Implement              :mil2, after mil1, 14d
Sprint-0305              :spr0305, after spr2904,4d

section To Do
Sprint-0905              :crit, active, spr0905, after spr0305,10d
Improvement             :mil3, after mil2, 10d
Sprint-1905              :spr1905, after spr0905,10d
Report                  :mil4, after mil3, 6d
Sprint-2705             :spr2705, after spr1905, 6d
```

## Contributor

| ID       | Name               | Email                        |
| -------- | --------           | --------                     |
| 1        | Hong Thanh Hoai    | 1612855@student.hcmus.edu.vn |
| 2        | Huynh Minh Huan    | 1612858@student.hcmus.edu.vn |
| 3        | Bui Thuy Vy        | 1612831@student.hcmus.edu.vn |
